import numpy
import math
import pickle

class NeuralNetwork(object):
    def __init__(self,
                 train_df,
                 test_df,
                 size_of_nn=[784,16,16,10],
                 weights_path = "weight_and_biases/zero-one-one-relu-softmax/weights",
                 biases_path = "weight_and_biases/zero-one-one-relu-softmax/biases",
                ):
        
        """
        size_of_nn merupakan sebuah array 1 dimensi yang menunjukkan jumlah neuron dari masing-masing layer.
        Diasumsikan bahwa elemen pertama array merepresentasikan input layer, dan elemen terakhir merepresentasikan output layer.

        weights merupakan sebuah array yang berisi (jumlah layer - 1) matriks, dimana matriks ke-i merepresentasikan
        hubungan antar layer ke-i dengan layer ke-(i+1), dimana tiap matriks berukuran (jumlah neuron layer i+1) X (jumlah neuron layer i+1),
        dengan tiap elemen (k, j) berisi weight hubungan neuron ke-j di layer ke-i dengan neuron ke-k di layer ke-(i+1).

        biases merupakan sebuah array yang berisi (jumlah layer - 1) vektor vertikal, dimana tiap vektor berisikan daftar bias untuk neuron-neuron
        dalam suatu layer. Input layer tidak memiliki bias.
        """
        self.train_df = train_df
        self.test_df = test_df
        self.size_of_nn = size_of_nn
        self.num_of_layers = len(size_of_nn)
        self.weights_path = weights_path
        self.biases_path = biases_path
        try:
            self.weights = pickle.load(open(weights_path, "rb"))
            print("Weights loaded from", weights_path)
            self.biases = pickle.load(open(biases_path, "rb"))
            print("Biases loaded from", biases_path)
        except:
            print("Loading error! Regenerating weights and biases")
            ## Sigmoid weights
            #self.weights = [numpy.random.randn(y, x) for x, y in zip(size_of_nn[:-1], size_of_nn[1:])]
            #self.biases = [numpy.random.randn(x, 1) for x in size_of_nn[1:]]
            ## ReLU weights
            self.weights = [numpy.random.randn(y, x) * 0.1 for x, y in zip(size_of_nn[:-1], size_of_nn[1:])]
            self.biases = [numpy.random.randn(x, 1) * 0.1 for x in size_of_nn[1:]]
        
    def feed_forward(self, inp):
        """
        Melakukan feed forward input kedalam neural network untuk mendapatkan output dari neural network.
        input merupakan sebuah vektor vertikal berukuran jumlah neuron pada input layer.
        Contoh : Pada NN dengan input layer 3 neuron, maka salah satu input yang valid adalah [[1], [2], [3]]

        Hasil keluaran berupa sebuah vektor vertikal berukuran jumlah neuron pada output layer.
        Contoh : Pada NN dengan output layer 2 neuron, maka salah satu output yang mungkin terjadi adalah
        [[1.5], [2.5]]
        """
        num_of_layer = len(self.weights)
        result = inp
        for index, (weight, bias) in enumerate(zip(self.weights, self.biases)):
            result = numpy.dot(weight, result) + bias
            #result = sigmoid(result)
            if index == num_of_layer - 1:
                result = softmax(result)
            else:
                result = ReLU(result)
        return result
    
    def evaluate(self):
        """
        Melakukan evaluasi
        """
        total_correct_ans = 0
        for index, row in self.test_df.iterrows():
            label = row.iloc[0]
            pixels = numpy.array(row.iloc[1:].tolist()).reshape((-1, 1))
            result = self.feed_forward(pixels)
            expected = numpy.zeros((10, 1))
            expected[label] = 1
            total_correct_ans += int(expected[numpy.argmax(result)][0])
        print("evaluation result : accuracy =", total_correct_ans * 100.00 / test_df.shape[0], "%")    
    
    def SGD(self, mini_batch_size, epoch_limit, train_rate, log_level=0):
        batches = math.floor(self.train_df.shape[0] / mini_batch_size)
        epoch_index = 0
        while epoch_index < epoch_limit:
            epoch_train_df = self.train_df.sample(frac=1)
            epoch_correct_ans = 0
            epoch_loss = 0
            for i in range(0, batches):
                batch_loss = 0
                batch_correct_ans = 0
                batch_gradient_weights = [numpy.zeros(w.shape) for w in self.weights]
                batch_gradient_biases = [numpy.zeros(b.shape) for b in self.biases]
                start_idx = i * mini_batch_size
                for j in range(start_idx, start_idx + mini_batch_size):
                    row = epoch_train_df.iloc[j]
                    label = row.iloc[0]
                    pixels = numpy.array(row.iloc[1:].tolist()).reshape((-1, 1))
                    expected = numpy.zeros((10, 1))
                    expected[label] = 1
                    
                    backprop_result = self.back_propagation(pixels, expected)
                    batch_gradient_weights = [w + dw for w, dw in zip(batch_gradient_weights, backprop_result[0])] 
                    batch_gradient_biases = [b + db for b, db in zip(batch_gradient_biases, backprop_result[1])] 
                    batch_correct_ans += backprop_result[2]
                    batch_loss += backprop_result[3]
                    epoch_loss += backprop_result[3]
            
                self.weights = [old_w - ((train_rate / mini_batch_size) * grad_w)
                                for old_w, grad_w in zip(self.weights, batch_gradient_weights)]
                self.biases = [old_b - ((train_rate / mini_batch_size) * grad_b)
                                for old_b, grad_b in zip(self.biases, batch_gradient_biases)]
                epoch_correct_ans += batch_correct_ans
                if log_level >= 2:
                    print("avg loss of epoch", epoch_index + 1, "batch", i  + 1, "=", batch_loss / mini_batch_size)
                    print("correct ans in this batch", batch_correct_ans)
            if log_level >= 1:
                print("avg loss of epoch", epoch_index + 1, "=", epoch_loss / train_df.shape[0])
                print("epoch", epoch_index + 1, "done, total correct ans =", epoch_correct_ans, "| accuracy =", 100.00 * epoch_correct_ans / train_df.shape[0], "%")
            epoch_index += 1
            if (epoch_index % 5 == 0):
                train_rate *= 0.5
            pickle.dump(self.weights, open(self.weights_path, "wb"))
            pickle.dump(self.biases, open(self.biases_path, "wb"))
        print("done")
    
    def back_propagation(self, inp, expected):
        gradient_weights = [numpy.ones(w.shape) for w in self.weights]
        gradient_biases = [numpy.ones(b.shape) for b in self.biases]
        
        inp = numpy.divide(inp, numpy.float32(256)) # ReLU variant
        activations = [inp]
        results = []
        for weight, bias in zip(self.weights, self.biases):
            result = numpy.dot(weight, inp) + bias
            results.append(result)
            #inp = sigmoid(result)
            inp = ReLU(result)
            activations.append(inp)
        
        correct = int(expected[numpy.argmax(activations[-1])][0])
        loss = numpy.sum([((xx-yy)**2)/2 for xx, yy in zip(activations[-1], expected)])
        
        #grad = numpy.multiply(d_mean_square_loss(activations[-1], expected), d_sigmoid(results[-1])) # Sigmoid variant
        grad = d_mean_square_loss(activations[-1], expected) # ReLU variant
        gradient_biases[-1] = grad
        gradient_weights[-1] = numpy.dot(grad, activations[-2].transpose()) 
        for layer in range(2, self.num_of_layers):
            #grad = numpy.multiply(numpy.dot(self.weights[-layer+1].transpose(), grad), d_sigmoid(results[-layer])) # Sigmoid variant
            grad = numpy.multiply(numpy.dot(self.weights[-layer+1].transpose(), grad), d_ReLU(results[-layer])) # ReLU variant
            gradient_biases[-layer] = grad
            gradient_weights[-layer] = numpy.dot(grad, activations[-layer-1].transpose())
        return (gradient_weights, gradient_biases, correct, loss)
    
    def predict(self, pixels):
#         pixels = test_df.iloc[0].iloc[1:].tolist()
        pixels = numpy.array(pixels).reshape((-1, 1))
        max = 0
        best = -1
        for index, prec in enumerate(self.feed_forward(pixels)):
            if prec > max:
                max = prec
                best = index
        return best
    
def d_mean_square_loss(result, expected):
    return result - expected

def sigmoid(x):
    """
    Fungsi yang memetakan hasil persamaan ke dalam range 0..1, sekarang
    menggunakan sigmoid function.
    """
    return 1 / (1 + numpy.exp(-x))

def d_sigmoid(x):
    return numpy.multiply(sigmoid (x), (1 - sigmoid(x)))

def ReLU(x):
    return numpy.maximum(x, 0)

def d_ReLU(x):
    return (x > 0)

def softmax(x):
    total = numpy.sum(numpy.exp(x))
    return x/total