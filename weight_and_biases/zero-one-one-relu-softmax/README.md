Fully Connected NN Architecture:
768 Node Input Layer
16 Node Hidden Layer 1
16 Node Hidden Layer 2
10 Node Output Layer

Train Data Max Accuracy ~ 95%
Test Data Max Accuracy ~ 93%