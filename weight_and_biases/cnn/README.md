CNN Architecture:
5*5*16 Convolution Layer, Valid Padding
2*2 Max Pooling, Stride = 2
5*5*32 Convolution Layer, Valid Padding
2*2 Max Pooling, Stride = 2
512 to 10 Fully-Connected Layer

Train Data Max Accuracy : 89.7064951082518%
Test Data Max Accuracy : 82.72827282728272%