#!/usr/bin/env python
# coding: utf-8
# ## Convolutional Neural Network

# In[24]:


import numpy as np
import math
import pickle

class ConvolutionLayer(object):
    def __init__(self, length, width, channel, input_channel):
        self.length = length
        self.width = width
        self.channel = channel
        self.input_channel = input_channel
        
        scale = self.length * self.width * input_channel
        std_dev = np.sqrt(1/scale)
        self.filters = np.random.normal(loc=0, scale=std_dev, size=(self.channel, self.input_channel, self.length, self.width))
        self.d = np.zeros((self.channel, self.input_channel, self.length, self.width))
    
    def forward(self, inp):
        self.inp = inp
        (self.activation, self.result) = ConvolutionLayer.forward_jit(self.inp, self.length, self.width, self.channel, self.filters)
        return self.activation
    
    # @jit(nopython=True)
    def forward_jit(inp, length, width, channel, filters):
        (inp_channel, inp_length, inp_width) = inp.shape
        
        result_length = (inp_length - length) + 1
        result_width = (inp_width - width) + 1
        result = np.zeros((channel, result_length, result_width))
        
        for c in range(0, channel):
            for l in range(0, result_length):
                for w in range(0, result_width):
                    result[c, l, w] = np.sum(inp[:, l:l + length, w:w + width] * filters[c])
        activation = np.maximum(result, 0)
        return (activation, result)
    
    def backpropagation(self, grad):
        (newgrad, self.d) = ConvolutionLayer.backpropagation_jit(self.inp, self.length, self.width, self.channel, self.filters, self.d, grad)
        return newgrad
    
    # @jit(nopython=True)
    def backpropagation_jit(inp, length, width, channel, filters, d, grad):
        (inp_channel, inp_length, inp_width) = inp.shape
        result_length = (inp_length - length) + 1
        result_width = (inp_width - width) + 1
        
        newgrad = np.zeros((inp_channel, inp_length, inp_width))
        
        for c in range(0, channel):
            for l in range(0, result_length):
                for w in range(0, result_width):
                    d[c] += grad[c, l, w] * inp[:, l:l + length, w:w + width]
                    newgrad[:, l:l + length, w:w + width] = grad[c, l, w] * filters[c]
        newgrad = (newgrad > 0)
        return (newgrad, d)
    
    def update(self, train_rate=0.5, batch_size=1):
        self.filters = ConvolutionLayer.update_jit(train_rate, batch_size, self.channel, self.filters, self.d)
        
        # Reset derivative
        self.d = np.zeros((self.channel, self.input_channel, self.length, self.width))
    
    # @jit(nopython=True)
    def update_jit(train_rate, batch_size, channel, filters, d):
        for i in range(0, channel):
            filters[i] -= ((train_rate / batch_size) * d[i])
        return filters
    
    def save(self, path):
        pickle.dump(self.filters, open(path, "wb"))
    
    def load(self, path):
        self.filters = pickle.load(open(path, "rb"))
    
class PoolingLayer(object):
    def __init__(self, length, width):
        self.length = length
        self.width = width
        self.d = None
    
    def forward(self, inp):
        self.inp = inp
        self.result = PoolingLayer.forward_jit(self.inp, self.length, self.width)
        return self.result
    
    # @jit(nopython=True)    
    def forward_jit(inp, length, width):
        (inp_channel, inp_length, inp_width) = inp.shape
        
        result_length = int(inp_length / length)
        result_width = int(inp_width / width)
        result = np.zeros((inp_channel, result_length, result_width))
        
        for c in range(0, inp_channel):
            l = 0
            while (l < inp_length):
                w = 0
                while (w < inp_width):
                    result[c, int(l / length), int(w / width)] = np.max(inp[c, l:l + length, w:w + width])
                    w += width
                l += length
        
        return result
    
    def backpropagation(self, grad):
        return PoolingLayer.backpropagation_jit(self.inp, self.length, self.width, grad)
    
    # @jit
    def backpropagation_jit(inp, length, width, grad):
        (inp_channel, inp_length, inp_width) = inp.shape
        result_length = int(inp_length / length)
        result_width = int(inp_width / width)
        
        reshaped = grad.reshape((inp_channel, result_length, result_width))
        dprev = np.zeros((inp_channel, inp_length, inp_width))
        
        for c in range(0, inp_channel):
            l = 0
            while (l < inp_length):
                w = 0
                while (w < inp_width):
                    (x, y) = np.unravel_index(np.argmax(inp[c, l:l + width, w:w + width]), (length, width))
                    dprev[c, l + x, w + y] = reshaped[c, int(l / length), int(w / width)]
                    w += width
                l += length
        return dprev
    
    def update(self, train_rate=0.5, batch_size=1):
        return
    
    def save(self, path):
        return
    
    def load(self, path):
        return
    
class FCLayer(object):
    def __init__(self, length, width):
        self.length = length
        self.width = width
        
        self.weights = 0.01 * np.random.rand(self.width, self.length)
        self.d = np.zeros((self.width, self.length))
    
    def forward(self, inp):
        (inp_channel, inp_length, inp_width) = inp.shape
        self.inp = inp.reshape((inp_channel * inp_length * inp_width, 1))
        
        (self.activation, self.result) = FCLayer.forward_jit(self.inp, self.weights)
        
        return self.activation
    
    # @jit
    def forward_jit(inp, weights):
        result = np.dot(weights, inp)
        activation = np.maximum(result, 0)
        
        return (activation, result)
    
    def backpropagation(self, grad):
        (self.d, newgrad) = FCLayer.backpropagation_jit(self.inp, self.weights, self.d, grad)
        return newgrad
    
    # @jit
    def backpropagation_jit(inp, weights, d, grad):
        d += np.dot(grad, inp.T)
        newgrad = np.multiply(np.dot(weights.T, grad), (inp > 0))
        return (d, newgrad)
    
    def update(self, train_rate=0.5, batch_size=1):
        self.weights -= ((train_rate / batch_size) * self.d)
        
        # Reset derivative
        self.d = np.zeros((self.width, self.length))
    
    def save(self, path):
        pickle.dump(self.weights, open(path, "wb"))
    
    def load(self, path):
        self.weights = pickle.load(open(path, "rb"))
        
class ConvolutionalNeuralNetwork(object):
    def __init__(self, layers):
        self.layers = layers
    
    def forward(self, inp):
        self.result = inp
        for layer in self.layers:
            self.result = layer.forward(self.result)
        return self.result
    
    def backpropagation(self, expected):
        grad = d_mean_square_loss(self.result, expected)
        for i in range(1, len(self.layers) + 1):
            grad = self.layers[-i].backpropagation(grad)
    
    def update(self, train_rate, batch_size):
        for i in range(1, len(self.layers) + 1):
            self.layers[-i].update(train_rate, batch_size)
    
    def SGD(self, batch_size, epoch_limit, train_rate, log_level=0):
        batches = math.floor(train_df.shape[0] / batch_size)
        epoch_index = 0
        while epoch_index < epoch_limit:
            epoch_train_df = train_df.sample(frac=1)
            epoch_correct_ans = 0
            epoch_loss = 0
            for i in range(0, batches):
                batch_loss = 0
                batch_correct_ans = 0
                start_idx = i * batch_size
                for j in range(start_idx, start_idx + batch_size):
                    row = epoch_train_df.iloc[j]
                    label = row.iloc[0]
                    pixels = np.reshape(row.iloc[1:].tolist(), (1, 28, 28))
                    pixels = (pixels - 33)/78
                    expected = np.zeros((10, 1))
                    expected[label] = 1
                    
                    self.forward(pixels)
                    batch_correct_ans += int(expected[np.argmax(self.result)])
                    batch_loss += mean_square_loss(self.result, expected)
                    
                    self.backpropagation(expected)
            
                self.update(train_rate, batch_size)
                epoch_correct_ans += batch_correct_ans
                if log_level >= 2:
                    print("avg loss of epoch", epoch_index + 1, "batch", i  + 1, "=", batch_loss / batch_size)
                    print("correct ans in this batch", batch_correct_ans)
                if (i % 100 == 0):
                    self.save()
            if log_level >= 1:
                print("avg loss of epoch", epoch_index + 1, "=", epoch_loss / train_df.shape[0])
                print("epoch", epoch_index + 1, "done, total correct ans =", epoch_correct_ans, "| accuracy =", 100.00 * epoch_correct_ans / train_df.shape[0], "%")
            epoch_index += 1
            if (epoch_index % 5 == 0):
                train_rate *= 0.5
            self.save()
        print("done")
    
    def evaluate(self):
        total_correct_ans = 0
        for index, row in test_df.iterrows():
            label = row.iloc[0]
            pixels = np.reshape(row.iloc[1:].tolist(), (1, 28, 28))
            pixels = (pixels - 33)/78
            expected = np.zeros((1, 10))
            expected[0, label] = 1
            
            self.forward(pixels)

            total_correct_ans += int(expected[0, np.argmax(self.result)])
            if (index % 100 == 0):
                print("currently tested : ", index)
                print(total_correct_ans)
        print("evaluation result : accuracy =", total_correct_ans * 100.00 / test_df.shape[0], "%")        
    
    def load(self):
        path = "weight_and_biases/cnn/"
        for i in range(0, len(self.layers)):
            load_path = path + str(i)
            self.layers[i].load(load_path)
            
    def save(self):
        path = "weight_and_biases/cnn/"
        for i in range(0, len(self.layers)):
            save_path = path + str(i)
            self.layers[i].save(save_path)

def mean_square_loss(result, expected):
    return np.sum([((xx-yy)**2)/2 for xx, yy in zip(result, expected)])

def d_mean_square_loss(result, expected):
    return result - expected

def ReLU(x):
    return np.maximum(x, 0)

def d_ReLU(x):
    return (x > 0)

def softmax(x):
    total = np.sum(np.exp(x))
    return x/total