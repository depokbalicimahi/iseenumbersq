from tkinter import *
from tkinter.colorchooser import askcolor
from PIL import Image
from neural_network import NeuralNetwork
import numpy as np
import math
import cv2
import random
from scipy import ndimage
from convolutional_neural_network import ConvolutionLayer, PoolingLayer, FCLayer, ConvolutionalNeuralNetwork

class Paint(object):

    SIZE = 56
    PEN_SCALE = 0.1
    DEFAULT_COLOR = 'black'

    def __init__(self):
        self.root = Tk()

        self.nn = NeuralNetwork(None, None)

        CL1 = ConvolutionLayer(5, 5, 16, 1)
        Pool1 = PoolingLayer(2, 2)
        CL2 = ConvolutionLayer(5, 5, 32, 16)
        Pool2 = PoolingLayer(2, 2)
        FC1 = FCLayer(512, 10)
        self.cnn = ConvolutionalNeuralNetwork([CL1, Pool1, CL2, Pool2, FC1])
        self.cnn.load()

        self.pen_size = self.SIZE*self.PEN_SCALE

        self.eraser_button = Button(self.root, text='reset', command=self.use_eraser)
        self.eraser_button.grid(row=4, column=6, columnspan=6, rowspan=2,  sticky='nesw')

        self.predict_button = Button(self.root, text='predict', command=self.predict)
        self.predict_button.grid(row=2, column=6, columnspan=6, rowspan=2, sticky='nesw')

        self.c = Canvas(self.root, bg='white', width=10*self.SIZE, height=10*self.SIZE)
        self.c.grid(row=0, column=0, columnspan=5, rowspan=5)

        self.prec_var = StringVar()
        self.prec_var.set('')
        self.prediction = Label(self.root, textvariable=self.prec_var, width=5, font=("Courier", 60))
        self.prediction.grid(row=0, column=6, columnspan=6, rowspan=2, sticky='nesw')

        self.setup()
        self.root.mainloop()

    def setup(self):
        self.old_x = None
        self.old_y = None
        self.line_width = self.pen_size
        self.color = self.DEFAULT_COLOR
        self.eraser_on = False
        self.c.bind('<B1-Motion>', self.paint)
        self.c.bind('<ButtonRelease-1>', self.reset)
    
    def predict(self):
        img = self.get_canvas_img(self.c, "ccd")
        img = img.resize((28,28), Image.ANTIALIAS)
        pixels = list(img.getdata())
        pixels = [255-x[0] for x in pixels]
        pixels = self.center_image(pixels)

        counter = 0
        for pixel in pixels:
            if counter == 28:
                print()
                counter = 0
            print(str(pixel).zfill(3)[0]*2,end='')
            counter+=1

        prediction = self.nn.predict(pixels)
        img.save("input.jpg")
        if prediction == -1 or prediction == 3:
            pred = prediction
            pixels = np.reshape(pixels, (1, 28, 28)).astype(np.float32)
            pixels = (pixels - 33) / 78
            result = self.cnn.forward(pixels)
            prediction = np.argmax(result)
            if pred == 3 and prediction != 9:
                prediction = 3
            if prediction == -1:
                prediction = random.randint(0,9)

        self.prec_var.set(str(prediction))

    def center_image(self, pixels):
        pixels = np.array(pixels).reshape(28,28).astype(np.uint8)
        (retval, grid) = cv2.threshold(pixels, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

        while np.sum(grid[0]) == 0:
            grid = grid[1:]

        while np.sum(grid[:,0]) == 0:
            grid = np.delete(grid,0,1)

        while np.sum(grid[-1]) == 0:
            grid = grid[:-1]

        while np.sum(grid[:,-1]) == 0:
            grid = np.delete(grid,-1,1)
        
        rows,cols = grid.shape
        print(grid)

        if rows > cols:
            factor = 20.0/rows
            rows = 20
            cols = int(round(cols*factor))
            grid = cv2.resize(grid, (cols,rows))
        else:
            factor = 20.0/cols
            cols = 20
            rows = int(round(rows*factor))
            grid = cv2.resize(grid, (cols, rows))

        colsPadding = (int(math.ceil((28-cols)/2.0)),int(math.floor((28-cols)/2.0)))
        rowsPadding = (int(math.ceil((28-rows)/2.0)),int(math.floor((28-rows)/2.0)))
        grid = np.lib.pad(grid,(rowsPadding,colsPadding),'constant')
        
        grid = self.best_shift(grid)
        print(grid)
        
        # return grid.flatten()
        return grid

        
    def best_shift(self, grid):
        cy,cx = ndimage.measurements.center_of_mass(grid)
        rows,cols = grid.shape

        shiftx = np.round(cols/2.0-cx).astype(int)
        shifty = np.round(rows/2.0-cy).astype(int)

        translate_matrix = np.float32([[1,0,shiftx], [0,1,shifty]])
        shifted = cv2.warpAffine(grid, translate_matrix, (cols, rows))

        return shifted


    def use_eraser(self):
        self.c.delete("all")
        # self.activate_button(self.eraser_button, eraser_mode=True)

    def activate_button(self, some_button, eraser_mode=False):
        self.active_button.config(relief=RAISED)
        some_button.config(relief=SUNKEN)
        self.active_button = some_button
        self.eraser_on = eraser_mode

    def paint(self, event):
        self.line_width = self.pen_size
        paint_color = 'white' if self.eraser_on else self.color
        if self.old_x and self.old_y:
            self.c.create_line(self.old_x, self.old_y, event.x, event.y,
                               width=self.line_width, fill=paint_color,
                               capstyle=ROUND, smooth=TRUE, splinesteps=36)
        self.old_x = event.x
        self.old_y = event.y

    def reset(self, event):
        self.old_x, self.old_y = None, None

    def get_canvas_img(self,canvas,fileName):
        canvas.postscript(file = fileName + '.eps') 
        return Image.open(fileName + '.eps') 

if __name__ == '__main__':
    Paint()